package com.master.lib.utils

import android.media.AudioManager
import android.os.Build
import com.master.lib.ext.application

/**
 * 音量控制工具类
 * @author: MasterChan
 * @date: 2023-01-06 10:13
 */
object VolumeUtils {

    /**
     * 获取音量大小
     * @param streamType
     * * AudioManager.STREAM_VOICE_CALL
     * * AudioManager.STREAM_SYSTEM
     * * AudioManager.STREAM_RING
     * * AudioManager.STREAM_MUSIC
     * * AudioManager.STREAM_ALARM
     * * AudioManager.STREAM_NOTIFICATION
     * * AudioManager.STREAM_DTMF
     * * AudioManager.STREAM_ACCESSIBILITY
     * @return Int
     */
    fun getVolume(streamType: Int): Int {
        return application.getSystemService(AudioManager::class.java).getStreamVolume(streamType)
    }

    /**
     * 设置音量大小
     * @param streamType
     * * AudioManager.STREAM_VOICE_CALL
     * * AudioManager.STREAM_SYSTEM
     * * AudioManager.STREAM_RING
     * * AudioManager.STREAM_MUSIC
     * * AudioManager.STREAM_ALARM
     * * AudioManager.STREAM_NOTIFICATION
     * * AudioManager.STREAM_DTMF
     * * AudioManager.STREAM_ACCESSIBILITY
     * @param volume 音量大小
     * @param flags
     * * AudioManager.FLAG_SHOW_UI
     * * AudioManager.FLAG_ALLOW_RINGER_MODES
     * * AudioManager.FLAG_PLAY_SOUND
     * * AudioManager.FLAG_REMOVE_SOUND_AND_VIBRATE
     * * AudioManager.FLAG_VIBRATE
     */
    fun setVolume(streamType: Int, volume: Int, flags: Int) {
        try {
            application.getSystemService(AudioManager::class.java)
                .setStreamVolume(streamType, volume, flags)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getMaxVolume(streamType: Int): Int {
        return application.getSystemService(AudioManager::class.java).getStreamMaxVolume(streamType)
    }

    fun getMinVolume(streamType: Int): Int {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            application.getSystemService(AudioManager::class.java).getStreamMinVolume(streamType)
        } else 0
    }
}