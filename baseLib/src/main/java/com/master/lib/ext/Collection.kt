package com.master.lib.ext

inline fun <T> Collection<T>.isNotEmpty(predicate: Collection<T>.() -> Unit) = apply {
    if (isNotEmpty()) predicate(this)
}

inline fun <T> Collection<T>.isEmpty(predicate: Collection<T>.() -> Unit) = apply {
    if (isEmpty()) predicate(this)
}
