package com.master.lib.ext

import android.annotation.SuppressLint
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.text.method.DigitsKeyListener
import android.text.method.LinkMovementMethod
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import com.master.lib.widget.span.ExtendImageSpan

fun TextView.setText(@StringRes res: Int, vararg format: Any) {
    text = resources.getString(res, format)
}

fun TextView.addUnderLine() {
    paint.flags = paint.flags or Paint.UNDERLINE_TEXT_FLAG
    paint.isAntiAlias = true
}

fun TextView.addDeleteLine() {
    paint.flags = paint.flags or Paint.STRIKE_THRU_TEXT_FLAG
    paint.isAntiAlias = true
}

fun TextView.removeUnderLine() {
    paintFlags = paintFlags and Paint.UNDERLINE_TEXT_FLAG.inv()
}

fun TextView.removeDeleteLine() {
    paintFlags = paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
}

fun TextView.bold() {
    paint.isFakeBoldText = true
    paint.isAntiAlias = true
}

fun TextView.font(fontName: String, fontFolder: String = "fonts") {
    typeface = Typeface.createFromAsset(context.assets, "$fontFolder/$fontName")
}

fun TextView.setDrawableLeft(drawable: Drawable?) {
    drawable?.setBounds(0, 0, drawable.minimumWidth, drawable.minimumHeight)
    setCompoundDrawables(drawable, null, null, null)
}

fun TextView.setDrawableTop(drawable: Drawable?) {
    drawable?.setBounds(0, 0, drawable.minimumWidth, drawable.minimumHeight)
    setCompoundDrawables(null, drawable, null, null)
}

fun TextView.setDrawableRight(drawable: Drawable?) {
    drawable?.setBounds(0, 0, drawable.minimumWidth, drawable.minimumHeight)
    setCompoundDrawables(null, null, drawable, null)
}

fun TextView.setDrawableBottom(drawable: Drawable?) {
    drawable?.setBounds(0, 0, drawable.minimumWidth, drawable.minimumHeight)
    setCompoundDrawables(null, null, null, drawable)
}

fun TextView.setDigits(digits: String) {
    keyListener = DigitsKeyListener.getInstance(digits)
}

fun TextView.appendSizeSpan(text: CharSequence, textSize: Float) = apply {
    append(text.toSizeSpan(IntRange(0, text.length), textSize))
}

fun TextView.appendColorSpan(text: CharSequence, color: Int) = apply {
    append(text.toColorSpan(IntRange(0, text.length), color))
}

fun TextView.appendBackgroundColorSpan(text: CharSequence, color: Int) = apply {
    append(text.toBackgroundColorSpan(IntRange(0, text.length), color))
}

fun TextView.appendStrikethroughSpan(text: CharSequence) = apply {
    append(text.toStrikethroughSpan(IntRange(0, text.length)))
}

fun TextView.appendUnderlineSpan(text: CharSequence) = apply {
    append(text.toUnderlineSpan(IntRange(0, text.length)))
}

fun TextView.appendClickSpan(text: CharSequence, color: Int = 0, clickListener: (View) -> Unit) =
    apply {
        movementMethod = LinkMovementMethod.getInstance()
        //去掉点击后的背景颜色
        highlightColor = Color.TRANSPARENT
        append(text.toClickSpan(IntRange(0, text.length), color, clickListener))
    }

fun TextView.appendStyleSpan(text: CharSequence, style: Int) = apply {
    append(text.toStyleSpan(style = style, range = IntRange(0, text.length)))
}

fun TextView.appendTypeFaceSpan(text: CharSequence, typeface: Typeface) = apply {
    append(text.toTypeFaceSpan(IntRange(0, text.length), typeface))
}

fun TextView.appendImageSpan(
    drawable: Drawable,
    verticalAlignment: Int = ExtendImageSpan.ALIGN_CENTER,
    imageWidth: Int = -1,
    imageHeight: Int = -1,
    leftMargin: Int = 0,
    rightMargin: Int = 0
) = apply {
    append(
        text.toImageSpan(
            IntRange(0, text.length), drawable, verticalAlignment, leftMargin, rightMargin,
            imageWidth, imageHeight
        )
    )
}

fun TextView.appendImageSpan(
    @DrawableRes res: Int,
    verticalAlignment: Int = ExtendImageSpan.ALIGN_CENTER,
    imageWidth: Int = -1,
    imageHeight: Int = -1,
    leftMargin: Int = 0,
    rightMargin: Int = 0
) = apply {
    appendImageSpan(
        getDrawable(res)!!, verticalAlignment, imageWidth, imageHeight, leftMargin, rightMargin
    )
}

var EditText.isEditable: Boolean
    get() {
        return isFocusable && isLongClickable && isFocusableInTouchMode
    }
    @SuppressLint("ClickableViewAccessibility")
    set(value) {
        isFocusable = value
        isLongClickable = value
        isFocusableInTouchMode = value
        if (!value) {
            val gestureDetector = GestureDetector(context, object :
                GestureDetector.SimpleOnGestureListener() {
                override fun onSingleTapConfirmed(e: MotionEvent?): Boolean {
                    performClick()
                    return true
                }
            })
            setOnTouchListener { _, event ->
                gestureDetector.onTouchEvent(event)
                true
            }
        } else {
            setOnTouchListener(null)
        }
    }