package com.master.lib.ext

import java.util.*

fun Long.toDate(): Date {
    return Date(this)
}