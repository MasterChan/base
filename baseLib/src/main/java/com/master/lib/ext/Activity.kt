package com.master.lib.ext

import android.app.Activity
import android.widget.FrameLayout

fun Activity.contentView(): FrameLayout = findViewById(android.R.id.content)