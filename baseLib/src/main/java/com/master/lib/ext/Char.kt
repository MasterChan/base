package com.master.lib.ext

/**
 * 16进制转10进制
 * @receiver Char
 * @return Int
 */
fun Char.hex2dec(): Int {
    return when (this) {
        in '0'..'9' -> {
            this - '0'
        }
        in 'A'..'F' -> {
            this - 'A' + 10
        }
        else -> {
            throw IllegalArgumentException()
        }
    }
}