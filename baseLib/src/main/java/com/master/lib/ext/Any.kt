package com.master.lib.ext

import android.os.Looper

fun dp2px(dp: Float): Float = dp * application.displayDensity + 0.5f
fun dp2px(dp: Int): Float = dp * application.displayDensity + 0.5f
fun px2dp(px: Float): Float = px / application.displayDensity + 0.5f
fun px2dp(px: Int): Float = px / application.displayDensity + 0.5f
fun dp2pxi(dp: Float): Int = (dp * application.displayDensity + 0.5f).toInt()
fun dp2pxi(dp: Int): Int = (dp * application.displayDensity + 0.5f).toInt()
fun px2dpi(px: Float): Int = (px / application.displayDensity + 0.5f).toInt()
fun px2dpi(px: Int): Int = (px / application.displayDensity + 0.5f).toInt()

fun sp2px(sp: Float): Float = sp * application.scaledDensity + 0.5f
fun sp2px(sp: Int): Float = sp * application.scaledDensity + 0.5f
fun px2sp(px: Float): Float = px / application.scaledDensity + 0.5f
fun px2sp(px: Int): Float = px / application.scaledDensity + 0.5f
fun sp2pxi(sp: Float): Int = (sp * application.scaledDensity + 0.5f).toInt()
fun sp2pxi(sp: Int): Int = (sp * application.scaledDensity + 0.5f).toInt()
fun px2spi(px: Float): Int = (px / application.scaledDensity + 0.5f).toInt()
fun px2spi(px: Int): Int = (px / application.scaledDensity + 0.5f).toInt()

val isMainThread: Boolean = Looper.myLooper() == Looper.getMainLooper()

val packageName: String = application.packageName

/**
 * 判断输入的条件是否成立，如果成立返回原对象，不成立返回null
 * @receiver T
 * @param condition [@kotlin.ExtensionFunctionType] Function1<T, Boolean>
 * @return T?
 */
inline fun <T> T.orNull(condition: T.() -> Boolean): T? {
    return if (condition.invoke(this)) return this else null
}

/**
 * 同时与多个参数比较是否相等
 * @receiver T
 * @param args Array<out T>
 * @return Boolean
 */
fun <T> T.equals(vararg args: T): Boolean {
    args.forEach { if (it != this) return false }
    return true
}